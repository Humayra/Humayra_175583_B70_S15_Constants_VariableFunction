<?php

    if(!isset($_SESSION)) session_start();  // in which page session wanna use, this line must be written there.
    
    session_destroy();
    header('Location: Session.php');